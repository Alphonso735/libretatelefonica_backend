/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Modelo;

/**
 *
 * @author roberto
 */
public class dotTelCompleto {
    private int numero;
    private int id_socio;
    private String tipoTelefono;

    public dotTelCompleto(int numero, int id_socio, String tipoTelefono) {
        this.numero = numero;
        this.id_socio = id_socio;
        this.tipoTelefono = tipoTelefono;
    }

    public dotTelCompleto() {
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getId_socio() {
        return id_socio;
    }

    public void setId_socio(int id_socio) {
        this.id_socio = id_socio;
    }

    public String getTipoTelefono() {
        return tipoTelefono;
    }

    public void setTipoTelefono(String tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }
    
    
}//finclase
