/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Modelo;

/**
 *
 * @author roberto
 */
public class Telefono {
    private long numero;
    private int id_contacto;
    private int id_tipo;

    public Telefono() {
    }

    public Telefono(long numero, int id_contacto, int id_tipo) {
        this.numero = numero;
        this.id_contacto = id_contacto;
        this.id_tipo = id_tipo;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }
    
  
    
    
}//finclase
