/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Servicios;

import com.LibretaTelefonica.Dao.ContactoDao;
import com.LibretaTelefonica.Dao.TelefonoDao;
import com.LibretaTelefonica.Modelo.Telefono;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author roberto
 */
@Path("contactos")
public class ContactoServicio {
    //listado de contactos
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContactos(){
        
        ContactoDao gestor=new ContactoDao();
        ArrayList datos=gestor.lstContactos();
        
        if(datos.isEmpty())
            return Response.status(Response.Status.NO_CONTENT).build();
        
        return Response.ok(datos).build();
    }
    
    //Listado de telefonos de un contacto
    @GET
    @Path("/{id_contacto}")
    public Response getTelefonoContacto(@PathParam("id_contacto") int id_contacto){
        TelefonoDao gestorTelef= new TelefonoDao();
        ArrayList datos=gestorTelef.lstTelefonosXContacto(id_contacto);
        
        if(datos.isEmpty())
            return Response.status(Response.Status.NO_CONTENT).build();
        
        return Response.ok(datos).build();
    }
    
    //Insertar un telefono a un contacto
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveTelefono(Telefono telefono){
        TelefonoDao gestorTelef=new TelefonoDao();
        System.out.println("entro");
        if(gestorTelef.insertTelefono(telefono))
            return Response.ok().build();
        else
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
}//finclase
