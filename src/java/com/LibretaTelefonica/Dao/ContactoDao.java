/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Dao;

import com.LibretaTelefonica.Modelo.Contacto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */
public class ContactoDao extends Dao {
    
    //listado de contactos
    public ArrayList<Contacto> lstContactos(){
        ArrayList<Contacto> resultado=new ArrayList<Contacto>();
        //--------------------
        String consulta="SELECT * FROM Contactos;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Contacto(
                    datos.getInt(1),
                    datos.getString(2)                                          
                ));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(ContactoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //--------------------
        return resultado;
    }
    
    //obtener un contacto por su id
    public Contacto getContacto(int id){
        Contacto resultado=null;
        //-----------------------
        String consulta="SELECT * FROM Contactos WHERE id=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, id);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=new Contacto(
                    datos.getInt(1),
                    datos.getString(2)
            );
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(ContactoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //-----------------------
        return resultado;
    }
    
    //insertar un contacto
    public boolean insertContacto(String nombre_contacto){
        boolean resultado=false;
        //-----------------------
        String consulta="INSERT INTO Contactos (nombre) VALUES (?);";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        
        try {
            stmt.setString(1, nombre_contacto);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();          
        } catch (SQLException ex) {
            Logger.getLogger(ContactoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        //-----------------------
        return resultado;
    }
    
    //borrar un contacto por su id
    public boolean eraseContacto(int id){
        boolean resultado=false;
        //-----------------------
        String consulta="DELETE FROM Contactos WHERE id=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, id);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(ContactoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //-----------------------
        return resultado;
    }
    
    //modificar un contacto
    public boolean updateContacto(Contacto contacto){
        boolean resultado=false;
        //----------------------------
        String consulta="UPDATE Contacto SET nombre=? WHERE id=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setString(1, contacto.getNombre());
            stmt.setInt(2, contacto.getId());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(ContactoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //----------------------------
        return resultado;
    }
}//finclase
