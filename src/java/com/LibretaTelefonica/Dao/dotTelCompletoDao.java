/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Dao;

import com.LibretaTelefonica.Modelo.dotTelCompleto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */
public class dotTelCompletoDao extends Dao {
    
    //listado de telefonos segun un id Socio
    public ArrayList<dotTelCompleto> lstTelefonosCompleto(int id_contacto){
        ArrayList<dotTelCompleto> resultado=new ArrayList<dotTelCompleto>();
        //------------------------
        String consulta="SELECT t.numero,t.id_contacto,tip.nombre FROM Telefonos t JOIN Tipo_Numero tip ON t.id_tipo=tip.id WHERE t.id_contacto=?;";
        PreparedStatement stmt= super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, id_contacto);
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new dotTelCompleto(
                        datos.getInt(1),
                        datos.getInt(2),
                        datos.getString(3)
                ));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(dotTelCompletoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //------------------------
        return resultado;
    }
}//finclase
