/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */
public class Dao {
    private Connection conexion;
    private String url;
    private String user;
    private String pass;

    public Dao() {
        this.url = "jdbc:mariadb://localhost:3306/LibretaTelefonica?zeroDateTimeBehavior=convertToNull";
        this.user = "roberto";
        this.pass = "123";
    }
    
    protected PreparedStatement obtenerConexion(String consulta){
        PreparedStatement stmt=null;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            this.conexion=DriverManager.getConnection(this.url,this.user,this.pass);
            stmt=this.conexion.prepareStatement(consulta);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stmt;
    }
    
    protected void cerrarConexion(){
        try {
            this.conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}//finclase
