/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Dao;

import com.LibretaTelefonica.Modelo.TipoNumero;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */
public class TipoNumeroDao extends Dao {
    
    //listado de tipos
    public ArrayList<TipoNumero> lstTipoNumeros(){
        ArrayList<TipoNumero> resultado=new ArrayList<TipoNumero>();
        //----------------
        String consulta="SELECT * FROM Tipo_Numero;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new TipoNumero(
                        datos.getInt(1),
                        datos.getString(2)
                ));                
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoNumeroDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //----------------
        return resultado;
    }
    
    //insertar un tipo 
    public boolean insertTipoNumero(String nombre_tipo){
        boolean resultado=false;
        //-------------------
        String consulta="INSERT INTO TipoNumero (nombre) VALUES(?);";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setString(1, nombre_tipo);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoNumeroDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //-------------------
        return resultado;
    }
    
    //borrar un tipo
    public boolean eraseTipoNumero(int id_tipo_numero){
        boolean resultado=false;
        //----------------
        String consulta="DELETE FROM TipoNumero WHERE id=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, id_tipo_numero);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoNumeroDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //----------------
        return resultado;
    }
    
    //modificar un tipo
    public boolean updateTipoNumero(TipoNumero tipoNumero){
        boolean resultado=false;
        //----------------
        String consulta="UPDATE Tipo_Numero SET nombre=? WHERE id=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setString(1, tipoNumero.getNombre());
            stmt.setInt(2, tipoNumero.getId());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoNumeroDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //----------------
        return resultado;
    }
    
    //obtener un tipo segun un id
    public TipoNumero getTipoNumero(int id){
        TipoNumero resultado=null;
        //---------------------
        String consulta="SELECT * FROM Tipo_Numero WHERE id=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=new TipoNumero(
                    datos.getInt(1),
                    datos.getString(2)
            );
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoNumeroDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //---------------------
        return resultado;
    }
}//finclase
