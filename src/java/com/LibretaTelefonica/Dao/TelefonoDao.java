/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LibretaTelefonica.Dao;

import com.LibretaTelefonica.Modelo.Telefono;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */
public class TelefonoDao extends Dao {
    
    //listado de telefonos
    public ArrayList<Telefono> lstTelefonos(){
        ArrayList<Telefono> resultado=new ArrayList<Telefono>();
        //----------------------
        String consulta="SELECT * FROM Telefonos;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Telefono(
                        datos.getInt(1),                        
                        datos.getInt(2),                        
                        datos.getInt(3)                        
                ));              
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TelefonoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //----------------------
        return resultado;
    }
    
    //insertar un numero 
    public boolean insertTelefono(Telefono telefono){
        boolean resultado=false;
        //------------------------
        String consulta="INSERT INTO Telefonos (numero,id_contacto,id_tipo) VALUES(?,?,?);";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setLong(1, telefono.getNumero());
            stmt.setInt(2, telefono.getId_contacto());
            stmt.setInt(3, telefono.getId_tipo());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TelefonoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //------------------------
        return resultado;
    }
    
    //obtener un telefono 
    public Telefono getTelefono(int numero){
        Telefono resultado=null;
        //------------------
        String consulta="SELECT * FROM Telefonos WHERE numero=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, numero);
            ResultSet datos=stmt.executeQuery();
            datos.next();
            resultado=new Telefono(
                    datos.getInt(1),
                    datos.getInt(2),
                    datos.getInt(3)
            );
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TelefonoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //------------------
        return resultado;
    }
    
    //borrar un telefono
    public boolean eraseTelefono(int numero){
        boolean resultado=false;
        //-----------------
        String consulta="DELETE FROM Telefonos WHERE numero=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, numero);
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TelefonoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //-----------------
        return resultado;
    }
    
    //modificar un telefono
    public boolean updateTelefono(Telefono telefono){
        boolean resultado=false;
        //---------------
        String consulta="UPDATE Telefonos SET id_contacto=?, id_tipo=? WHERE numero=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, telefono.getId_contacto());
            stmt.setInt(2, telefono.getId_tipo());
            stmt.setLong(3, telefono.getNumero());
            resultado=(stmt.executeUpdate()>0)?true:resultado;
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TelefonoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //---------------
        return resultado;
    }
    
    //listado de telefonos para un id_contacto
    public ArrayList<Telefono> lstTelefonosXContacto(int id_contacto){
        ArrayList<Telefono> resultado=new ArrayList<Telefono>();
        //------------------
        String consulta="SELECT * FROM Telefonos WHERE id_contacto=?;";
        PreparedStatement stmt=super.obtenerConexion(consulta);
        try {
            stmt.setInt(1, id_contacto);
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Telefono(
                        datos.getLong(1),
                        datos.getInt(2),
                        datos.getInt(3)
                ));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TelefonoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.cerrarConexion();
        //------------------
        return resultado;
    }
    
    //listado de telefonos segun un tipo
    public ArrayList<Telefono> lstTelefonoXTipo(int id_tipo){
        ArrayList<Telefono> resultado=new ArrayList<Telefono>();
        //--------------------
        String consula="SELECT * FROM Telefonos WHERE id_tipo=?;";
        PreparedStatement stmt=super.obtenerConexion(consula);
        try {
            stmt.setInt(1, id_tipo);
            ResultSet datos=stmt.executeQuery();
            while(datos.next()){
                resultado.add(new Telefono(
                        datos.getInt(1),
                        datos.getInt(2),
                        datos.getInt(3)
                ));
            }
            datos.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(TelefonoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        //--------------------
        return resultado;
    }
}//finclase
